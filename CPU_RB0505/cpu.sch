EESchema Schematic File Version 5
EELAYER 33 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
Comment5 ""
Comment6 ""
Comment7 ""
Comment8 ""
Comment9 ""
$EndDescr
Connection ~ 6250 2150
Connection ~ 6350 1850
Connection ~ 6450 1750
NoConn ~ 6150 1650
Entry Wire Line
	2400 1150 2500 1050
Entry Wire Line
	2400 1250 2500 1150
Entry Wire Line
	2400 1350 2500 1250
Entry Wire Line
	2400 1450 2500 1350
Entry Wire Line
	2400 1550 2500 1450
Entry Wire Line
	2400 1650 2500 1550
Entry Wire Line
	2400 1750 2500 1650
Entry Wire Line
	2400 1850 2500 1750
Entry Wire Line
	2400 1950 2500 1850
Entry Wire Line
	2400 2050 2500 1950
Entry Wire Line
	2400 2150 2500 2050
Entry Wire Line
	2400 2250 2500 2150
Entry Wire Line
	2400 2350 2500 2250
Entry Wire Line
	2400 2450 2500 2350
Entry Wire Line
	2400 2550 2500 2450
Entry Wire Line
	2400 2650 2500 2550
Entry Wire Line
	2400 3150 2500 3050
Entry Wire Line
	2400 3250 2500 3150
Entry Wire Line
	2400 3350 2500 3250
Entry Wire Line
	2400 3450 2500 3350
Entry Wire Line
	2400 3550 2500 3450
Entry Wire Line
	2400 3650 2500 3550
Entry Wire Line
	2400 3750 2500 3650
Entry Wire Line
	2400 3850 2500 3750
Entry Wire Line
	2400 3950 2500 3850
Entry Wire Line
	2400 4050 2500 3950
Entry Wire Line
	2400 4150 2500 4050
Entry Wire Line
	2400 4250 2500 4150
Entry Wire Line
	2400 4350 2500 4250
Entry Wire Line
	2400 4450 2500 4350
Entry Wire Line
	2400 4550 2500 4450
Entry Wire Line
	2400 4650 2500 4550
Entry Wire Line
	2400 5250 2500 5150
Entry Wire Line
	2400 5350 2500 5250
Entry Wire Line
	2400 5450 2500 5350
Entry Wire Line
	2400 5550 2500 5450
Wire Wire Line
	2250 1150 2400 1150
Wire Wire Line
	2250 1250 2400 1250
Wire Wire Line
	2250 1350 2400 1350
Wire Wire Line
	2250 1450 2400 1450
Wire Wire Line
	2250 1550 2400 1550
Wire Wire Line
	2250 1650 2400 1650
Wire Wire Line
	2250 1750 2400 1750
Wire Wire Line
	2250 1850 2400 1850
Wire Wire Line
	2250 1950 2400 1950
Wire Wire Line
	2250 2050 2400 2050
Wire Wire Line
	2250 2150 2400 2150
Wire Wire Line
	2250 2250 2400 2250
Wire Wire Line
	2250 2350 2400 2350
Wire Wire Line
	2250 2450 2400 2450
Wire Wire Line
	2250 2550 2400 2550
Wire Wire Line
	2250 2650 2400 2650
Wire Wire Line
	2250 3150 2400 3150
Wire Wire Line
	2250 3250 2400 3250
Wire Wire Line
	2250 3350 2400 3350
Wire Wire Line
	2250 3450 2400 3450
Wire Wire Line
	2250 3550 2400 3550
Wire Wire Line
	2250 3650 2400 3650
Wire Wire Line
	2250 3750 2400 3750
Wire Wire Line
	2250 3850 2400 3850
Wire Wire Line
	2250 3950 2400 3950
Wire Wire Line
	2250 4050 2400 4050
Wire Wire Line
	2250 4150 2400 4150
Wire Wire Line
	2250 4250 2400 4250
Wire Wire Line
	2250 4350 2400 4350
Wire Wire Line
	2250 4450 2400 4450
Wire Wire Line
	2250 4550 2400 4550
Wire Wire Line
	2250 4650 2400 4650
Wire Wire Line
	2250 5250 2400 5250
Wire Wire Line
	2250 5350 2400 5350
Wire Wire Line
	2250 5450 2400 5450
Wire Wire Line
	2250 5550 2400 5550
Wire Wire Line
	2250 5650 2400 5650
Wire Wire Line
	2250 5750 2400 5750
Wire Wire Line
	2250 5850 2400 5850
Wire Wire Line
	2250 5950 2400 5950
Wire Wire Line
	2250 6500 2400 6500
Wire Wire Line
	2250 6600 2400 6600
Wire Wire Line
	2250 6700 2400 6700
Wire Wire Line
	2250 6800 2400 6800
Wire Wire Line
	2250 6900 2400 6900
Wire Wire Line
	2250 7000 2400 7000
Wire Wire Line
	2250 7100 2400 7100
Wire Wire Line
	2250 7200 2400 7200
Wire Wire Line
	5000 950  5250 950 
Wire Wire Line
	5000 1050 5250 1050
Wire Wire Line
	5000 1150 5250 1150
Wire Wire Line
	5000 1250 5250 1250
Wire Wire Line
	5000 1350 5250 1350
Wire Wire Line
	5000 1450 5250 1450
Wire Wire Line
	5000 1550 5250 1550
Wire Wire Line
	5000 1650 5250 1650
Wire Wire Line
	5000 1750 6450 1750
Wire Wire Line
	5000 1850 6350 1850
Wire Wire Line
	5000 1950 6750 1950
Wire Wire Line
	5000 2050 6750 2050
Wire Wire Line
	5000 2150 6250 2150
Wire Wire Line
	5000 2250 5250 2250
Wire Wire Line
	5000 2350 5250 2350
Wire Wire Line
	5000 2450 5250 2450
Wire Wire Line
	6150 1200 6150 1250
Wire Wire Line
	6250 1650 6250 2150
Wire Wire Line
	6250 2150 6750 2150
Wire Wire Line
	6350 1650 6350 1850
Wire Wire Line
	6350 1850 6750 1850
Wire Wire Line
	6450 1650 6450 1750
Wire Wire Line
	6450 1750 6750 1750
Wire Bus Line
	2500 1050 2500 1250
Wire Bus Line
	2500 1250 2500 1350
Wire Bus Line
	2500 1350 2500 1550
Wire Bus Line
	2500 1550 2500 1650
Wire Bus Line
	2500 1650 2500 1850
Wire Bus Line
	2500 1850 2500 1950
Wire Bus Line
	2500 1950 2500 2150
Wire Bus Line
	2500 2150 2500 2350
Wire Bus Line
	2500 2350 2500 2550
Wire Bus Line
	2500 3050 2500 3150
Wire Bus Line
	2500 3050 2600 3050
Wire Bus Line
	2500 3150 2500 3350
Wire Bus Line
	2500 3350 2500 3550
Wire Bus Line
	2500 3550 2500 3750
Wire Bus Line
	2500 3750 2500 3850
Wire Bus Line
	2500 3850 2500 4050
Wire Bus Line
	2500 4050 2500 4250
Wire Bus Line
	2500 4250 2500 4450
Wire Bus Line
	2500 4450 2500 5150
Wire Bus Line
	2500 5150 2500 5250
Wire Bus Line
	2500 5250 2500 5450
Wire Bus Line
	2600 1050 2500 1050
Text Label 2250 1150 0    50   ~ 0
D0
Text Label 2250 1250 0    50   ~ 0
D1
Text Label 2250 1350 0    50   ~ 0
D2
Text Label 2250 1450 0    50   ~ 0
D3
Text Label 2250 1550 0    50   ~ 0
D4
Text Label 2250 1650 0    50   ~ 0
D5
Text Label 2250 1750 0    50   ~ 0
D6
Text Label 2250 1850 0    50   ~ 0
D7
Text Label 2250 1950 0    50   ~ 0
D8
Text Label 2250 2050 0    50   ~ 0
D9
Text Label 2250 2150 0    50   ~ 0
D10
Text Label 2250 2250 0    50   ~ 0
D11
Text Label 2250 2350 0    50   ~ 0
D12
Text Label 2250 2450 0    50   ~ 0
D13
Text Label 2250 2550 0    50   ~ 0
D14
Text Label 2250 2650 0    50   ~ 0
D15
Text Label 2250 3150 0    50   ~ 0
A0
Text Label 2250 3250 0    50   ~ 0
A1
Text Label 2250 3350 0    50   ~ 0
A2
Text Label 2250 3450 0    50   ~ 0
A3
Text Label 2250 3550 0    50   ~ 0
A4
Text Label 2250 3650 0    50   ~ 0
A5
Text Label 2250 3750 0    50   ~ 0
A6
Text Label 2250 3850 0    50   ~ 0
A7
Text Label 2250 3950 0    50   ~ 0
A8
Text Label 2250 4050 0    50   ~ 0
A9
Text Label 2250 4150 0    50   ~ 0
A10
Text Label 2250 4250 0    50   ~ 0
A11
Text Label 2250 4350 0    50   ~ 0
A12
Text Label 2250 4450 0    50   ~ 0
A13
Text Label 2250 4550 0    50   ~ 0
A14
Text Label 2250 4650 0    50   ~ 0
A15
Text Label 2250 5250 0    50   ~ 0
A16
Text Label 2250 5350 0    50   ~ 0
A17
Text Label 2250 5450 0    50   ~ 0
A18
Text Label 2250 5550 0    50   ~ 0
A19
Text HLabel 2400 5650 2    50   Input ~ 0
PST3
Text HLabel 2400 5750 2    50   Input ~ 0
PST4
Text HLabel 2400 5850 2    50   Input ~ 0
PST6
Text HLabel 2400 5950 2    50   Output ~ 0
A23
Text HLabel 2400 6500 2    50   Output ~ 0
CS0
Text HLabel 2400 6600 2    50   Output ~ 0
CS1
Text HLabel 2400 6700 2    50   Output ~ 0
CS2
Text HLabel 2400 6800 2    50   Output ~ 0
CS3
Text HLabel 2400 6900 2    50   Output ~ 0
CS4
Text HLabel 2400 7000 2    50   Output ~ 0
SCLK_SPI_1
Text HLabel 2400 7100 2    50   Output ~ 0
MTSR_SPI_1
Text HLabel 2400 7200 2    50   Input ~ 0
MRST_SPI_1_E
Text HLabel 2600 1050 2    50   Output ~ 0
D[0..15]
Text HLabel 2600 3050 2    50   Output ~ 0
A[0..19]
Text HLabel 5250 950  2    50   Input ~ 0
SPEED_MEASURE
Text HLabel 5250 1050 2    50   Output ~ 0
BRAKE
Text HLabel 5250 1150 2    50   Input ~ 0
DEF-TURB
Text HLabel 5250 1250 2    50   Output ~ 0
ENABLE-TURB
Text HLabel 5250 1350 2    50   Output ~ 0
WDOG
Text HLabel 5250 1450 2    50   Input ~ 0
+3.3V-FAILURE
Text HLabel 5250 1550 2    50   Input ~ 0
+5VREF-FAILURE
Text HLabel 5250 1650 2    50   Input ~ 0
+10VREF-FAILURE
Text HLabel 5250 2250 2    50   Output ~ 0
INV-STOP
Text HLabel 5250 2350 2    50   Output ~ 0
RAP-ALARM
Text HLabel 5250 2450 2    50   Output ~ 0
CMD_ME1
Text HLabel 6750 1750 2    50   Input ~ 0
BUSY-ME1
Text HLabel 6750 1850 2    50   Input ~ 0
BUSY-ME2
Text HLabel 6750 1950 2    50   Input ~ 0
USB_BUSY_E
Text HLabel 6750 2050 2    50   Input ~ 0
BUSY-AFF
Text HLabel 6750 2150 2    50   Input ~ 0
TEST-RAP-ALARM
$Comp
L power:+5V #PWR?
U 1 1 5E900EF9
P 6150 1200
F 0 "#PWR?" H 6150 1050 50  0001 C CNN
F 1 "+5V" H 6165 1373 50  0000 C CNN
F 2 "" H 6150 1200 50  0001 C CNN
F 3 "" H 6150 1200 50  0001 C CNN
	1    6150 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Network04 RN?
U 1 1 5E8FF62E
P 6350 1450
F 0 "RN?" H 6537 1495 50  0000 L CNN
F 1 "R_Network04" H 6537 1405 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP5" V 6625 1450 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 6350 1450 50  0001 C CNN
	1    6350 1450
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 8 1 5E9C336E
P 6750 3000
F 0 "U?" H 6758 3664 50  0000 C CNN
F 1 "ST10F276Z5" H 6758 3573 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 6800 3350 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 5250 3350 50  0001 C CNN
	8    6750 3000
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 7 1 5E9BD6D4
P 1550 6850
F 0 "U?" H 1558 7514 50  0000 C CNN
F 1 "ST10F276Z5" H 1558 7423 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 1600 7200 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 50  7200 50  0001 C CNN
	7    1550 6850
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 9 1 5E9C4E56
P 6650 4500
F 0 "U?" H 6550 3927 50  0000 C CNN
F 1 "ST10F276Z5" H 6550 3836 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 6700 4850 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 5150 4850 50  0001 C CNN
	9    6650 4500
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 5 1 5E9B574D
P 1400 5600
F 0 "U?" H 1567 6264 50  0000 C CNN
F 1 "ST10F276Z5" H 1567 6173 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 1450 5950 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H -100 5950 50  0001 C CNN
	5    1400 5600
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 1 1 5E9A9669
P 1750 1950
F 0 "U?" H 2045 3064 50  0000 C CNN
F 1 "ST10F276Z5" H 2045 2973 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 1800 2300 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 250 2300 50  0001 C CNN
	1    1750 1950
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 2 1 5E9ACC06
P 1700 3850
F 0 "U?" H 1755 4864 50  0000 C CNN
F 1 "ST10F276Z5" H 1755 4773 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 1750 4200 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 200 4200 50  0001 C CNN
	2    1700 3850
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 4 1 5E9B3194
P 5250 4000
F 0 "U?" H 5333 5114 50  0000 C CNN
F 1 "ST10F276Z5" H 5333 5023 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 5300 4350 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 3750 4350 50  0001 C CNN
	4    5250 4000
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 6 1 5E9B9492
P 4300 3750
F 0 "U?" H 4358 4614 50  0000 C CNN
F 1 "ST10F276Z5" H 4358 4523 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 4350 4100 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 2800 4100 50  0001 C CNN
	6    4300 3750
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 3 1 5E9AF82E
P 4200 1750
F 0 "U?" H 4255 2814 50  0000 C CNN
F 1 "ST10F276Z5" H 4255 2723 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 4250 2100 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 2700 2100 50  0001 C CNN
	3    4200 1750
	1    0    0    -1  
$EndComp
$Comp
L CPU_RB0505:ST10F276Z5 U?
U 10 1 5E9CAC86
P 9700 3850
F 0 "U?" H 9995 4964 50  0000 C CNN
F 1 "ST10F276Z5" H 9995 4873 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 9750 4200 50  0001 C CNN
F 3 "https://www.st.com/content/ccc/resource/technical/document/datasheet/92/18/5e/9c/68/61/42/f6/CD00115722.pdf/files/CD00115722.pdf/jcr:content/translations/en.CD00115722.pdf" H 8200 4200 50  0001 C CNN
	10   9700 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
